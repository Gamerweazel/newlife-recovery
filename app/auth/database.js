import Firebase from 'firebase'
import Auth0Lock from 'react-native-lock'
export const ref = new Firebase('https://newlife-5922a.firebaseio.com/')
export const lock = new Auth0Lock({
    clientId: '04odwAyiqcxjXtN3CDGVYDUXe01emoK6',
    domain: 'newlife.auth0.com'
})