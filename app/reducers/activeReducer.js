import * as types from '../actions/actions'
import _ from 'lodash'

export function userReducer(state = {}, action) {
    switch (action.type) {
        case types.LOGGEDIN_USER:
            return {...state, user: action.user, email: action.email }
        default:
            return state
    }
}

export function clientReducer(state = {}, action) {
    switch (action.type) {
        case types.FETCH_CLIENTS:
            return {...state, clients: _.sortBy(action.payload, function (el) {
                return el.val.Last_Name
            })
            }
        case types.CHANGE_ACTIVECLIENT:
            return {...state,
                selectedClient: {
                    id: action.selectedClient.id,
                    client: action.selectedClient.val,
                }
            }
        case types.DELETE_CLIENT:
            return {...state }
        case types.UPDATED_CLIENT:
            return {...state }
        default:
            return state
    }
}

export function noteReducer(state = {}, action) {
    switch (action.type) {
        case types.SELECTED_NOTE:
            return {...state, selectedNote: action.selectedNote }
        case types.UPDATED_NOTE:
            return {...state }
        case types.CREATE_NOTE:
            return {...state }
        case types.DELETE_NOTE:
            return {...state }
        case types.EMAIL_NOTE:
            return {...state }
        default:
            return state
    }
}

