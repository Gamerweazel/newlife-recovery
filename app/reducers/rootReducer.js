import { combineReducers } from 'redux'
import { 
  userReducer,
  clientReducer, 
  noteReducer
} from './activeReducer'
import interventions from './interventions'

const rootReducer = combineReducers({
  userReducer,
  interventions,
  clientReducer,
  noteReducer,
})

export default rootReducer