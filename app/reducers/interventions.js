export default function () {
    return [
        {
            name: 'Depression',
            presentations: [
                'depression presentation 1',
                'depression presentation 2'
            ],
            interventions: [
                'depression intervention 1',
                'depression intervention 2'
            ]
        },
        {
            name: 'Bipolar',
            presentations: [
                'Bipolar presentation 1',
                'Bipolar presentation 2'
            ],
            interventions: [
                'Bipolar intervention 1',
                'Bipolar intervention 2'
            ]
        },
        {
            name: 'Obsessive_Compulsive_Disorder',
            presentations: [
                'Obsessive-Compulsive_Disorder presentation 1',
                'Obsessive-Compulsive_Disorder presentation 2'
            ],
            interventions: [
                'Obsessive-Compulsive Disorder intervention 1',
                'Obsessive-Compulsive Disorder intervention 2'
            ]
        },
        {
            name: 'Trauma_Related_Disorder',
            presentations: [
                'Trauma/Stressor Related Disorder presentation 1',
                'Trauma/Stressor Related Disorder presentation 2',
                'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC'
            ],
            interventions: [
                'Trauma/Stressor Related Disorder intervention 1',
                'Trauma/Stressor Related Disorder intervention 2'
            ]
        },
        {
            name: 'Sleep_Wake_Disorder',
             presentations: [
                'Sleep-Wake Disorder presentation 1',
                'Sleep-Wake Disorder presentation 2'
            ],
            interventions: [
                'Sleep-Wake Disorder intervention 1',
                'Sleep-Wake Disorder intervention 2'
            ]
        },
        {
            name: 'Substance_Related_Disorder',
             presentations: [
                'Substance-Related Disorder presentation 1',
                'Substance-Related Disorder presentation 2'
            ],
            interventions: [
                'Substance-Related Disorder intervention 1',
                'Substance-Related Disorder intervention 2'
            ]
        },
        {
            name: 'Neurocognitive_Disorder',
             presentations: [
                'Neurocognitive Disorder presentation 1',
                'Neurocognitive Disorder presentation 2'
            ],
            interventions: [
                'Neurocognitive Disorder intervention 1',
                'Neurocognitive Disorder intervention 2'
            ]
        },
    ]
}