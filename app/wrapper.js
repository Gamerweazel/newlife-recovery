import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { AsyncStorage } from 'react-native'
import { applyMiddleware, createStore, compose } from 'redux'
import createLogger from 'redux-logger'
import thunk from 'redux-thunk'
import rootReducer from './reducers/rootReducer'
import App from './container-components/app'
import devTools from 'remote-redux-devtools'


const logger = createLogger()
const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(thunk, logger),
        devTools()
    )
)


const wrapper = () => {
    return (
        <Provider store={store}>
            <App />
        </Provider>
    )
}

export default wrapper