import { ref } from '../auth/database'
import base64 from 'base-64'
import querystring from 'querystring'

// Actions

export const LOGGEDIN_USER = 'LOGGEDIN_USER'
export const FETCH_CLIENTS = 'FETCH_CLIENTS'
export const CHANGE_ACTIVECLIENT = 'CHANGE_ACTIVECLIENT'
export const SELECTED_NOTE = 'SELECTED_NOTE'
export const UPDATED_NOTE = 'UPDATED_NOTE'
export const DELETE_CLIENT = 'DELETE_CLIENT'
export const CREATE_NOTE = 'CREATE_NOTE'
export const DELETE_NOTE = 'DELETE_NOTE'
export const EMAIL_NOTE = 'EMAIL_NOTE'

// Action Creators

export function loggedInUser(user, email) {
    return {
        type: LOGGEDIN_USER,
        user,
        email
    }
}

export function changeActiveClient(selectedClient) {
    return {
        type: CHANGE_ACTIVECLIENT,
        selectedClient
    }
}

export function fetchClients(userId) {
    return dispatch => {
        ref.child('users').child(userId).child('clients')
            .on('value', snapshot => {
                const clientList = []
                snapshot.forEach((child) => {
                    clientList.push({
                        id: child.key(),
                        val: child.val(),
                        notes: child.val().notes
                    })
                })
                dispatch({
                    type: FETCH_CLIENTS,
                    payload: clientList
                })
            })
    }
}

export function createClient(client, userId) {
    return dispatch => ref.child('users').child(userId).child('clients')
        .push(client)
}

export function deleteClient(id, userId) {
    let key = id
    return dispatch => {
        ref.child('users').child(userId).child('clients')
            .child(key).remove()
        dispatch({
            type: DELETE_CLIENT
        })
    }
}

export function selectedNote(selectedNote) {
    return {
        type: SELECTED_NOTE,
        selectedNote
    }
}

export function createNote(newNote, clientID, userId) {
    return dispatch => {
        ref.child('users').child(userId).child('clients')
            .child(clientID).child('notes').child(newNote.id)
            .set(newNote)
        dispatch({
            type: CREATE_NOTE,
            newNote
        })
    }
}

export function updatedNote(newNote, clientID, userId) {
    let id
    if (newNote.id === 1)
        id = (newNote.id - 1)
    else
        id = newNote.id
    return dispatch => {
        ref.child('users').child(userId).child('clients')
            .child(clientID).child('notes').child(id)
            .set(newNote)
    }
}

export function deleteNote(noteID, clientID, userId) {
    let id
    if (noteID === 1)
        id = (noteID - 1)
    else
        id = noteID
    return dispatch => {
        ref.child('users').child(userId).child('clients')
            .child(clientID).child('notes').child(id)
            .remove()
    }
}

export function emailNote(note, selectedClient, email) {
    return dispatch => {
        const body = querystring.stringify(
            {
                from: 'postmaster@sandbox0a8deb01924e47c6ba60cd655a5398be.mailgun.org',
                to: `${email}`,
                subject: `Progress Note ${selectedClient.client.First_Name + ' ' + selectedClient.client.Last_Name} ${note.date}`,
                html: `<div align='center'><img src='https://s4.postimg.org/z5cd9hvbh/before.png' height=100 width=350 align='center'><h1>Progress Note</h1></div><h2>Date: ${note.date}</h2><h2>Time & Duration:</h2><h2>Client Name: ${selectedClient.client.First_Name + ' ' + selectedClient.client.Last_Name}</h2><h3>Presenting Problem: ${note.problem}</h3><h4>${note.appliedPres.join('<br/>')}</h4><h3>Applied Interventions:</h3><h4>${note.appliedInts.join('<br/>')}</h4><h3>Plan:</h3><h4>${note.extraNotes}</h4><h3>Therapist:</h3>`
            })
        fetch('https://api.mailgun.net/v3/sandbox0a8deb01924e47c6ba60cd655a5398be.mailgun.org/messages', {
            method: 'POST',
            headers: {
                Authorization: 'Basic ' + (base64.encode('api:key-df70a9a8522da59ae2c462ecb193db5b')),
                'content-type': 'application/x-www-form-urlencoded'
            },
            body: body,
        }).then((response) => {
            console.log(response)
        }).catch((error) => {
            console.log(error)
        })
        dispatch({
            type: EMAIL_NOTE,
        })
    }
}
