import React, { Component } from 'react'
import {
    ScrollView,
    ToastAndroid,
    Modal,
    Dimensions,
    ListView,
    View,
    Text,
    TextInput,
    Picker,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { connect } from 'react-redux'
import _ from 'lodash'
import { updatedNote, deleteNote, emailNote } from '../actions/actions'
import ClientNotesPage from './clientNotesPage'
import moment from 'moment'

const Item = Picker.Item

// Container

class NotePage extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Interventions {...this.props} />
            </View>
        )
    }
}

function mapStateToprops(state) {
    return {
        user: state.userReducer.user,
        email: state.userReducer.email,
        selectedNote: state.noteReducer.selectedNote,
        problem: state.noteReducer.selectedNote.problem,
        problemsList: state.interventions,
        possiblePres: _.difference(_.find(state.interventions,
            { name: state.noteReducer.selectedNote.problem }).presentations,
            state.noteReducer.selectedNote.appliedPres),
        possibleInts: _.difference(_.find(state.interventions,
            { name: state.noteReducer.selectedNote.problem }).interventions,
            state.noteReducer.selectedNote.appliedInts),
        selectedClient: state.clientReducer.selectedClient
    }
}
export default connect(mapStateToprops)(NotePage)

const ds = new ListView.DataSource({
    rowHasChanged: (row1, row2) => row1 !== row2
})
class Interventions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            problem: this.props.problem,
            currentPresdataSource: ds.cloneWithRows(this.props.selectedNote
                .appliedPres),
            currentIntsdataSource: ds.cloneWithRows(this.props.selectedNote
                .appliedInts),
            availInts: ds.cloneWithRows(this.props.possibleInts),
            availPres: ds.cloneWithRows(this.props.possiblePres),
            availableInts: this.props.possibleInts,
            availablePres: this.props.possiblePres,
            appliedInts: this.props.selectedNote.appliedInts,
            appliedPres: this.props.selectedNote.appliedPres,
            text: this.props.selectedNote.extraNotes,
            modalVisible: false
        }
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    renderRowPres(rowData) {
        return (
            <View style={styles.row}>
                <TouchableOpacity
                    style={styles.rowButton}>
                    <View style={styles.rowWrapper}>
                        <Text style={styles.title}>{rowData}
                        </Text>
                        <Text></Text>
                        <Icon name='remove' size={40} onPress={() => this.deletePres(rowData) }/>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderRowInt(rowData) {
        return (
            <View style={styles.row}>
                <TouchableOpacity
                    style={styles.rowButton}>
                    <View style={styles.rowWrapper}>
                        <Text style={styles.title}>{rowData}
                        </Text>
                        <Text></Text>
                        <Icon name='remove' size={40} onPress={() => this.deleteInt(rowData) }/>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderRowModalAvailPres(rowData) {
        return (
            <View style={styles.row}>
                <TouchableOpacity
                    style={styles.rowButton}
                    onPress={this.renderRowModalAddPres.bind(this, rowData) }>
                    <View style={styles.rowWrapper}>
                        <Text style={styles.title}>{rowData}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderRowModalAvailInts(rowData) {
        return (
            <View style={styles.row}>
                <TouchableOpacity
                    style={styles.rowButton}
                    onPress={this.renderRowModalAddInts.bind(this, rowData) }>
                    <View style={styles.rowWrapper}>
                        <Text style={styles.title}>{rowData}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderRowModalAddPres(rowData) {
        let index
        const appliedListCopy = [...this.state.availablePres]
        for (let i = 0; i < appliedListCopy.length; i++) {
            if (appliedListCopy[i] === rowData) {
                index = i
                appliedListCopy.splice(index, 1)
                this.setState({
                    availablePres: appliedListCopy,
                    availPres: ds.cloneWithRows(appliedListCopy)
                })
            }
        }
        const listCopy = [...this.state.appliedPres]
        if (listCopy[0] === 'Your presentations will appear here') {
            listCopy.splice(0, 1, rowData)
        } else {
            listCopy.push(rowData)
        }
        this.setState({
            appliedPres: listCopy,
            currentPresdataSource: ds.cloneWithRows(listCopy)
        })
        ToastAndroid.show('Added to presentations', ToastAndroid.SHORT)
    }

    renderRowModalAddInts(rowData) {
        let index
        const appliedListCopy = [...this.state.availableInts]
        for (let i = 0; i < appliedListCopy.length; i++) {
            if (appliedListCopy[i] === rowData) {
                index = i
                appliedListCopy.splice(index, 1)
                this.setState({
                    availableInts: appliedListCopy,
                    availInts: ds.cloneWithRows(appliedListCopy)
                })
            }
        }
        const listCopy = [...this.state.appliedInts]
        if (listCopy[0] === 'Your interventions will appear here') {
            listCopy.splice(0, 1, rowData)
        } else {
            listCopy.push(rowData)
        }
        this.setState({
            appliedInts: listCopy,
            currentIntsdataSource: ds.cloneWithRows(listCopy)
        })
        ToastAndroid.show('Added to interventions', ToastAndroid.SHORT)
    }

    renderHeader(bool) {
        let title;
        if (bool === true)
            title = 'Add Client Presentations'
        else
            title = 'Add Client Interventions'
        return (
            <View>
                <Icon.Button
                    name="add"
                    backgroundColor="#2196F3"
                    justifyContent='center'
                    borderRadius={0}
                    onPress={() => this.setModalVisible(true) }>
                    {title}
                </Icon.Button>
            </View>
        )
    }

    renderHeaderAvail(bool) {
        let title;
        if (bool === true)
            title = 'Available Presentations:'
        else
            title = 'Available Interventions:'
        return (
            <Text style={[styles.Inttitle,
                {
                    backgroundColor: '#2196F3',
                    textAlign: 'center'
                }]}>
                {title}
            </Text>
        )
    }

    deletePres(rowData) {
        const listCopy = [...this.state.appliedPres]
        if (listCopy.length === 1) {
            ToastAndroid.show('Each note must have at least one presentation!', ToastAndroid.SHORT)
            return
        }
        let index;
        for (let i = 0; i < listCopy.length; i++) {
            if (listCopy[i] === rowData) {
                index = i
                listCopy.splice(index, 1)
            }
        }
        this.setState({
            appliedPres: listCopy,
            currentPresdataSource: ds.cloneWithRows(listCopy)
        })
        ToastAndroid.show('Presentation deleted', ToastAndroid.SHORT)
    }

    deleteInt(rowData) {
        const listCopy = [...this.state.appliedInts]
        if (listCopy.length === 1) {
            ToastAndroid.show('Each note must have at least one intervention!', ToastAndroid.SHORT)
            return
        }
        let index;
        for (let i = 0; i < listCopy.length; i++) {
            if (listCopy[i] === rowData) {
                index = i
                listCopy.splice(index, 1)
            }
        }
        this.setState({
            appliedInts: listCopy,
            currentIntsdataSource: ds.cloneWithRows(listCopy)
        })
        ToastAndroid.show('Intervention deleted', ToastAndroid.SHORT)
    }

    saveNote(id) {
        const { dispatch, selectedClient, user, email, selectedNote } = this.props
        const { text, appliedInts, appliedPres, problem } = this.state
        const m = moment()
        const date = m.format('MMMM Do YYYY')
        const newNote = {
            id,
            extraNotes: this.state.text,
            appliedPres: this.state.appliedPres,
            appliedInts: this.state.appliedInts,
            problem: this.state.problem,
            date: date

        }
        ToastAndroid.show('Note Saved!', ToastAndroid.SHORT)
        dispatch(updatedNote(newNote, selectedClient.id, user))
        dispatch(emailNote(newNote, selectedClient, email))
        this.props.navigator.pop({
            name: ClientNotesPage,
        })
    }

    render() {
        return (
            <View>
                <ListView
                    enableEmptySections={true}
                    style={styles.items}
                    dataSource={this.state.currentPresdataSource}
                    renderHeader={this.renderHeader.bind(this, true) }
                    renderRow={this.renderRowPres.bind(this) } />
                <ListView
                    enableEmptySections={true}
                    style={[styles.items, { marginTop: (deviceHeight / 35) }]}
                    dataSource={this.state.currentIntsdataSource}
                    renderHeader={this.renderHeader.bind(this, false) }
                    renderRow={this.renderRowInt.bind(this) } />
                <View>
                    <Modal
                        animationType={"slide"}
                        transparent={false}
                        visible={this.state.modalVisible}
                        onRequestClose={() => { } }
                        >
                        <ScrollView style={styles.scroll}>
                            <Text style={styles.text}>
                                Presenting Problem
                            </Text>
                            <Picker
                                selectedValue={this.state.problem}
                                onValueChange={(prob) => this.setState({
                                    problem: prob,
                                    availablePres: _.find(this.props.problemsList,
                                        { name: prob }).presentations,
                                    availableInts: _.find(this.props.problemsList,
                                        { name: prob }).interventions,
                                    availPres: ds.cloneWithRows(_.find(this.props.problemsList,
                                        { name: prob }).presentations),
                                    availInts: ds.cloneWithRows(_.find(this.props.problemsList,
                                        { name: prob }).interventions)
                                }) }>
                                <Picker.Item label='Depression' value='Depression' />
                                <Picker.Item label='Bipolar' value='Bipolar' />
                                <Picker.Item label='Obessesive Compulsive Disorder' value='Obessesive_Compulsive_Disorder' />
                                <Picker.Item label='Trama-Related Disorder' value='Trauma_Related_Disorder' />
                                <Picker.Item label='Sleep-Wake Disorder' value='Sleep_Wake_Disorder' />
                                <Picker.Item label='Substance Related' value='Substance_Related_Disorder' />
                                <Picker.Item label='Neurocognitive' value='Neurocognitive_Disorder' />
                            </Picker>
                            <ListView
                                enableEmptySections={true}
                                style={styles.Modalitems}
                                dataSource={this.state.availPres}
                                renderHeader={this.renderHeaderAvail.bind(this, true) }
                                renderRow={this.renderRowModalAvailPres.bind(this) } />
                            <ListView
                                enableEmptySections={true}
                                style={styles.Modalitems}
                                dataSource={this.state.availInts}
                                renderHeader={this.renderHeaderAvail.bind(this, false) }
                                renderRow={this.renderRowModalAvailInts.bind(this) } />
                            <View style={styles.subContainer}>
                                <Icon.Button
                                    name="done"
                                    backgroundColor="#E57373"
                                    justifyContent='center'
                                    onPress={() => this.setModalVisible(false) }>
                                    Done
                                </Icon.Button>
                            </View>
                        </ScrollView>
                    </Modal>
                    <View>
                        <Text style={styles.noteTitle}>
                            Notes:
                        </Text>
                        <TextInput style={styles.input}
                            placeholder={'Notes...'}
                            maxLength={180}
                            multiline={true}
                            numberOfLines={2}
                            value={this.state.text}
                            onChangeText={(text) => this.setState({ text }) } />
                    </View>
                    <View style={styles.subContainer}>
                        <Icon.Button
                            name="assignment-turned-in"
                            backgroundColor="#E57373"
                            justifyContent='center'
                            onPress={() => this.saveNote(this.props.selectedNote.id) }>
                            Save Progress Note
                        </Icon.Button>
                    </View>
                </View>
            </View>
        )
    }
}

let deviceHeight = Dimensions.get('window').height
let deviceWidth = Dimensions.get('window').width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    items: {
        marginTop: (deviceHeight / 12),
        height: (deviceHeight / 3.53),
        backgroundColor: '#fff',
        padding: 10,
    },
    Modalitems: {
        marginTop: 10,
        flex: 1,
        backgroundColor: '#fff',
        padding: 10,
    },
    row: {
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc',
        marginBottom: 10,
    },
    rowWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    rowButton: {
        padding: 10,
    },
    text: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: (deviceHeight / 25),
        backgroundColor: '#2196F3',
        fontSize: 15,
        color: '#E3F2FD',
        textAlign: 'center',
        padding: 10,
        fontWeight: 'bold',
    },
    title: {
        padding: 10,
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        color: '#4078C0',
        fontWeight: 'bold',
    },
    input: {
        marginLeft: 10,
        marginRight: 10
    },
    Inttitle: {
        padding: 10,
        flex: 1,
        color: '#E3F2FD',
        fontWeight: 'bold',
    },
    noteTitle: {
        marginTop: (deviceHeight / 35),
        marginLeft: 10,
        marginRight: 10,
        color: '#fff',
        fontWeight: 'bold',
        backgroundColor: '#BDBDBD',
        textAlign: 'center',
    },
    subContainer: {
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
    },
    scroll: {
        backgroundColor: '#fff'
    }
})