import React, { Component } from 'react'
import {
  Dimensions,
  TouchableHighlight,
  Text,
  StyleSheet,
  View,
  Navigator
} from 'react-native'
import MainContainer from './main'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialIcons';

const NavigationBarRouteMapper = {
  LeftButton(route, navigator, index, navState) {
    if (index > 0) {
      return (
        <TouchableHighlight
          underlayColor="transparent"
          onPress={() => { if (index > 0) { navigator.pop() } } }>
          <Text style={ styles.leftNavButtonText }>&laquo; </Text>
        </TouchableHighlight>)
    }
    else { return null }
  },
  RightButton(route, navigator, index, navState) {
    if (route.onPress) {
      return (
        <View style={styles.subContainer}>
          <TouchableHighlight
            underlayColor="transparent"
            onPress={ () => route.onPress() }>
            <Icon
              size={35}
              name="delete"
              color='white'  />
          </TouchableHighlight>
        </View>
      )
    } else if (index > 0) {
      return (
        <TouchableHighlight
          underlayColor="transparent"
          onPress={ () => { navigator.popToTop() } }>
          <Text style={ styles.rightNavButtonText }>
            {'Home'}
          </Text>
        </TouchableHighlight>)
    }
  },
  Title(route, navigator, index, navState) {
    return <Text style={ styles.title }>NewLifeRecovery</Text>
  }
};

export default class App extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Navigator
        initialRoute={{ name: MainContainer }}
        configureScene={this.configureScene}
        renderScene={this.renderScene}
        navigationBar={
          <Navigator.NavigationBar
            style={ styles.navbar }
            routeMapper={ NavigationBarRouteMapper } />}
        />
    )
  }

  renderScene(route, navigator) {
    let Component = route.name
    return (
      <Component navigator={navigator} {...route.passProps} />
    )
  }

  configureScene(route, routeStack) {
    return Navigator.SceneConfigs.PushFromRight
  }
}

let deviceWidth = Dimensions.get('window').width

const styles = StyleSheet.create({
  leftNavButtonText: {
    fontSize: 50,
    marginLeft: 13,
    marginTop: -10
  },
  rightNavButtonText: {
    fontSize: 16,
    marginRight: 13,
    marginTop: 18,
    color: '#E3F2FD'
  },
  navbar: {
    alignItems: 'center',
    backgroundColor: '#64B5F6',
  },
  title: {
    marginTop: 18,
    fontSize: 16,
    color: '#E3F2FD'
  },
  subContainer: {
    marginTop: 12,
    marginRight: 8,
    borderRadius: 5,
    backgroundColor: '#E57373'
  }
})



