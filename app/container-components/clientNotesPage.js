import React, { Component } from 'react'
import {
    ToastAndroid,
    Dimensions,
    ListView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux'
import _ from 'lodash'
import { selectedNote, createNote, deleteNote, fetchClients } from '../actions/actions'
import note from './note'
import moment from 'moment'

// Container

class ClientNotesPage extends Component {

    render() {
        return (
            <NotesList {...this.props} />
        )
    }
}

function mapStateToprops(state) {
    let index;
    for (let i = 0; i < state.clientReducer.clients.length; i++) {
        if (state.clientReducer.clients[i].id ===
            state.clientReducer.selectedClient.id)
            index = i
    }
    let obj = state.clientReducer.clients[index].notes
    let num = 0
    if (typeof state.clientReducer.clients[index].notes === 'object') {
        for (let key in obj) {
            num += 1
        }
    } else {
        num = obj.length
    }
    return {
        user: state.userReducer.user,
        selectedClient: state.clientReducer.selectedClient,
        notes: _.reverse(state.clientReducer.clients[index].notes),
        num: num
    }
}
export default connect(mapStateToprops)(ClientNotesPage)


class NotesList extends Component {
    constructor(props) {
        super(props)
        ds = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2
        })
        const { notes } = this.props
        this.state = {
            dataSource: ds.cloneWithRows(notes),
        }
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.notes !== this.props.notes) {
            this.setState({
                dataSource: this.state.dataSource.cloneWithRows(nextProps.notes)
            })
        }
    }

    selectNote(id) {
        const { dispatch, selectedClient, notes } = this.props
        const currentNote = (_.find(notes, { id: id }))
        dispatch(selectedNote(currentNote))
        this.props.navigator.push({
            name: note,
            onPress: () => this.deleteNote(id)
        })
    }

    createNote() {
        const { dispatch, selectedClient, user, notes } = this.props
        const m = moment()
        const clientID = selectedClient.id
        const newNote = {
            id: m.utc().toString(),
            problem: 'Depression',
            appliedPres: ['Your presentations will appear here'],
            appliedInts: ['Your interventions will appear here'],
            extraNotes: '',
            date: m.format('MMMM Do YYYY')
        }
        ToastAndroid.show('Note Created!', ToastAndroid.SHORT)
        dispatch(createNote(newNote, clientID, user))
        dispatch(selectedNote(newNote))
        this.props.navigator.push({
            name: note,
            onPress: () => this.deleteNote(newNote.id)
        })
    }

    deleteNote(id) {
        const { dispatch, notes, user, num } = this.props
        if (num === 1) {
            ToastAndroid.show('Each client must have at least one note!',
                ToastAndroid.SHORT)
            return
        }
        dispatch(deleteNote(id, this.props.selectedClient.id, user))
        this.props.navigator.pop()
        ToastAndroid.show('Note deleted', ToastAndroid.SHORT)
    }

    renderRow(rowData) {
        return (
            <View style={styles.row}>
                <TouchableOpacity
                    style={styles.rowButton}
                    onPress={this.selectNote.bind(this, rowData.id) } >
                    <View style={styles.rowWrapper}>
                        <Text style={styles.title}>{rowData.problem}...   ({rowData.date})
                        </Text>
                        <Icon name='create' size={25} />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderHeader() {
        return (
            <Text style={[
                {
                    backgroundColor: '#2196F3',
                    textAlign: 'center',
                    color: '#E3F2FD'
                }]}>
                Notes:
            </Text>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <ListView
                    enableEmptySections={true}
                    style={styles.items}
                    dataSource={this.state.dataSource}
                    renderRow={this.renderRow.bind(this) }
                    renderHeader={this.renderHeader.bind(this) } />
                <View style={styles.subContainer}>
                    <Icon.Button
                        name="add"
                        backgroundColor="#E57373"
                        justifyContent='center'
                        onPress={() => this.createNote() }>
                        New Note
                    </Icon.Button>
                </View>
            </View>

        )
    }
}

let deviceHeight = Dimensions.get('window').height

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: (deviceHeight / 12),
        backgroundColor: '#fff'
    },
    items: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 10,
    },
    row: {
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc',
        marginBottom: 10
    },
    rowWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    rowButton: {
        padding: 10,
    },
    text: {
        fontSize: 30,
        color: '#FF5722',
        textAlign: 'center',
    },
    title: {
        flex: 1,
        marginLeft: 10,
        color: '#4078C0',
        fontWeight: 'bold'
    },
    subContainer: {
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 6,
    },
})
