import React, { Component } from 'react'
import { connect } from 'react-redux'
import ClientContainer from './clients'
import { FormView } from './form'

// Container

class ManageClientContainer extends Component {

    render() {
        return (
            <FormView {...this.props} />
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.userReducer.user,
        selectedClient: state.clientReducer.selectedClient,
    }
}
export default connect(mapStateToProps)(ManageClientContainer)

