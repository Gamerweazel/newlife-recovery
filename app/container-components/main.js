import React, { Component } from 'react'
import {
    AsyncStorage,
    ActivityIndicator,
    Dimensions,
    Image,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import { connect } from 'react-redux'
import { loggedInUser, fetchClients } from '../actions/actions'
import ClientContainer from './clients'
import ManageClientPage from './manageClientPage'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { ref, lock } from '../auth/database'
const image = require('../images/logo.png')
const FirebaseTokenGenerator = require('firebase-token-generator')

// Container

class MainContainer extends Component {

    render() {
        return (
            <MainComponent {...this.props} />
        )
    }
}

function mapStateToProps(state) {
    return {
        token: state.userReducer.token,
        user: state.userReducer.user,
        clientList: state.clientReducer.clients
    }
}
export default connect(mapStateToProps)(MainContainer)

// Component

class MainComponent extends Component {
    constructor(props) {
        super(props)

        this.selectView = this.selectView.bind(this)
        this.selectCreate = this.selectCreate.bind(this)
        this.state = {
            user: this.props.user,
            animating: true,
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user !== this.props.user) {
            this.setState({
                user: nextProps.user
            })
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const { dispatch, token } = this.props
        if (prevProps.user !== this.props.user) {
            dispatch(fetchClients(this.props.user))
        }
    }

    selectView(props) {
        const { clientList } = this.props
        this.props.navigator.push({
            name: ClientContainer,
        })
    }

    selectCreate() {
        this.props.navigator.push({
            name: ManageClientPage,
        })
    }

    _login() {
        const { dispatch } = this.props
        lock.show({}, (err, profile, token) => {
            if (err) {
                console.log(err)
            }
            const tokenGenerator = new FirebaseTokenGenerator('3s7kubVWszUxXNFbYgZIhILhKbRy51NVXxIUtJDv')
            const ref_token = tokenGenerator.createToken({ uid: profile.userId })
            ref.authWithCustomToken(ref_token, function (error, authData) {
                if (error) {
                    console.log('Login Failed!')
                } else {
                    console.log('Login Successful!')
                    dispatch(loggedInUser(authData.uid, profile.email))
                }
            })
        })
    }

    render() {
        const { clientList } = this.props
        if (this.state.user) {
            if (!clientList) {
                return (
                    <View style={styles.container}>
                        <ActivityIndicator
                            animating={this.state.animating}
                            style={[styles.centering, { height: 80 }]}
                            size="large"
                            />
                        <Text style={[styles.title, styles.text]}>Fetching Clients...</Text>
                    </View>
                )
            } else {
                return (
                    <View style={styles.container}>
                        <Text></Text>
                        <View style={styles.subContainer}>
                            <Image
                                source={image}
                                style={{ flex: 1, width: Width, height: 100 }}
                                resizeMode='contain'
                                justifyContent='center'
                                />
                            <Text></Text>
                            <Icon.Button
                                justifyContent='center'
                                name="visibility"
                                backgroundColor="#E57373"
                                padding={15}
                                onPress={() => this.selectView() }>
                                View Notes
                            </Icon.Button>
                            <Text></Text>
                            <Text></Text>
                            <Icon.Button
                                justifyContent='center'
                                name="assignment-ind"
                                backgroundColor='#64B5F6'
                                padding={15}
                                onPress={() => this.selectCreate() }>
                                New Client
                            </Icon.Button>
                            <Text></Text>
                            <Text></Text>
                        </View>
                    </View>
                )
            }
        } else {
            return (
                <View style={styles.container}>
                    <View style={styles.subContainer}>
                        <Image
                            source={image}
                            style={{ flex: 1, width: Width, height: 100 }}
                            resizeMode='contain'
                            justifyContent='center'
                            />
                        <Text></Text>
                        <Text></Text>
                        <Icon.Button
                            justifyContent='center'
                            name="face"
                            backgroundColor="#E57373"
                            padding={15}
                            onPress={() => this._login() }>
                            Login
                        </Icon.Button>
                    </View>
                </View>
            )
        }
    }
}

let Width = (Dimensions.get('window').width / 1.2)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    centering: {
        alignItems: 'center',
        padding: 8,
    },
    subContainer: {
        justifyContent: 'center',
        marginLeft: 25,
        marginRight: 25
    },
    text: {
        fontSize: 30,
        color: '#FF5722',
        textAlign: 'center',
    }
})