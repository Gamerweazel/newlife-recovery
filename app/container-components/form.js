import React, { Component } from 'react';
import {
    Alert,
    ToastAndroid,
    Dimensions,
    StyleSheet,
    Text,
    TextInput,
    View,
    ScrollView,
} from 'react-native';
import { createClient } from '../actions/actions'
import ClientContainer from './clients'
import Icon from 'react-native-vector-icons/MaterialIcons'
import moment from 'moment'

const alphaExp = /^[a-zA-Z]+$/

export class FormView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            first: '',
            last: '',
            description: '',
        }
    }

    throwAlert() {
        Alert.alert(
            'Field Invalid',
            'One or more of your fields is invalid or empty'
        )
    }

    create(userId) {
        const client = {}
        client.First_Name = this.state.first
        client.Last_Name = this.state.last
        client.Description = this.state.description
        const { dispatch } = this.props
        const m = moment()
        for (let key in client) {
            if (client.key === '') {
                this.throwAlert()
                return
            }
        }
        if (client.First_Name.match(alphaExp) &&
            client.Last_Name.match(alphaExp)) {
            client.notes = [
                {
                    id: 1,
                    problem: 'Depression',
                    appliedInts: ['Your interventions will appear here'],
                    appliedPres: ['Your presentations will appear here'],
                    extraNotes: '',
                    date: m.format('MMMM Do YYYY')
                }
            ]
            ToastAndroid.show('Added ' + client.First_Name + ' ' +
                client.Last_Name, ToastAndroid.LONG)
            dispatch(createClient(client, userId))
            this.props.navigator.replace({
                name: ClientContainer,
            })
        } else {
            this.throwAlert()
            return
        }
    }
    render() {
        const { user } = this.props
        return (
            <View style={styles.container}>
                <ScrollView keyboardShouldPersistTaps={true}
                    style={{ paddingLeft: 10, paddingRight: 10, height: 200 }}>
                    <Text style={styles.text}>
                        Client Information
                    </Text>
                    <TextInput
                        style={styles.input}
                        value={this.state.first}
                        placeholder={'First Name'}
                        onChangeText={(text) => this.setState({ first: text }) } />
                    <TextInput
                        style={styles.input}
                        value={this.state.last}
                        placeholder={'Last Name'}
                        onChangeText={(text) => this.setState({ last: text }) } />
                    <TextInput
                        style={styles.input}
                        value={this.state.description}
                        placeholder={'additional information'}
                        onChangeText={(text) => this.setState({ description: text }) } />
                    <Icon.Button
                        name="assignment-turned-in"
                        backgroundColor="#E57373"
                        justifyContent='center'
                        onPress={() => this.create(user) }>
                        Save Client
                    </Icon.Button>
                </ScrollView>
            </View>
        )
    }
}

let deviceHeight = Dimensions.get('window').height

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        marginTop: (deviceHeight / 10),
        backgroundColor: '#fff'
    },
    text: {
        backgroundColor: '#2196F3',
        fontSize: 20,
        color: '#E3F2FD',
        textAlign: 'center',
    },
    input: {
        margin: 10,
        paddingLeft: 10
    },
})