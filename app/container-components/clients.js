import React, { Component } from 'react'
import {
    ToastAndroid,
    Dimensions,
    ListView,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux'
import _ from 'lodash'
import {
    changeActiveClient,
    deleteClient,
    fetchNotes
} from '../actions/actions'
import ClientNotesPage from './clientNotesPage'

// Container

class ClientContainer extends Component {

    render() {
        return (
            <ClientList {...this.props} />
        )
    }
}

function mapStateToprops(state) {
    return {
        user: state.userReducer.user,
        clientList: state.clientReducer.clients
    }
}
export default connect(mapStateToprops)(ClientContainer)

// Component


class ClientList extends Component {
    constructor(props) {
        super(props)
        const { clientList } = this.props
        this.changeSelected = this.changeSelected.bind(this)
        this.delete = this.delete.bind(this)
        this.ds = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2
        })
        this.state = {
            dataSource: this.ds.cloneWithRows(clientList),
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.clientList !== this.props.clientList) {
            this.setState({
                dataSource: this.state.dataSource.
                    cloneWithRows(nextProps.clientList)
            })
        }
    }

    changeSelected(id) {
        const { clientList, dispatch } = this.props
        const selectedClient = (_.find(clientList, { id: id }))
        dispatch(changeActiveClient(selectedClient))
        this.props.navigator.push({
            name: ClientNotesPage,
        })
    }

    delete(rowData, userId) {
        const { dispatch } = this.props
        dispatch(deleteClient(rowData.id, userId))
        ToastAndroid.show('Deleted ' + rowData.val.First_Name + ' ' +
            rowData.val.Last_Name, ToastAndroid.LONG)
    }

    renderRow(rowData) {
        let first = rowData.val.First_Name
        let last = rowData.val.Last_Name
        return (
            <View style={styles.row}>
                <TouchableOpacity
                    style={styles.rowButton}
                    onPress={() => this.changeSelected(rowData.id) } >
                    <View style={styles.rowWrapper}>
                        <Text style={styles.title}>
                            {last} {first}
                        </Text>
                        <Icon name='delete' size={25} onPress={() =>
                            this.delete(rowData, this.props.user) }/>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderHeader() {
        return (
            <Text style={[
                {
                    backgroundColor: '#2196F3',
                    textAlign: 'center',
                    color: '#E3F2FD'
                }]}>
                Clients:
            </Text>
        )
    }

    render() {
        if (this.props.clientList == 0) {
            return (
                <View style={styles.container}>
                    <Text style={[styles.title, styles.centering]}>
                        You have no clients!
                    </Text>
                </View>
            )
        } else {
            return (
                <View style={styles.container}>
                    <ListView
                        enableEmptySections={true}
                        style={styles.items}
                        dataSource={this.state.dataSource}
                        renderRow={this.renderRow.bind(this) }
                        renderHeader={this.renderHeader.bind(this) } />
                </View>
            )
        }
    }
}

let deviceHeight = Dimensions.get('window').height

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: (deviceHeight / 12),
    },
    centering: {
        textAlign: 'center',
        marginTop: 10
    },
    items: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 10,
    },
    moreButtonText: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: 'bold'
    },
    row: {
        backgroundColor: '#ffffff',
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc',
        marginBottom: 10
    },
    rowWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    rowButton: {
        padding: 10,
    },
    text: {
        fontSize: 30,
        color: '#FF5722',
        textAlign: 'center',
    },
    title: {
        flex: 1,
        marginLeft: 10,
        color: '#4078C0',
        fontWeight: 'bold'
    },
    thumbnail: {
        width: 120,
        height: 90
    }
})